/**
 * @author - Don Moore
 * @version - Intermediate programming project v1
 * @task - Create a code challenge which involves writing code challenges from the Logic Problem interface.
 *       - We are to write tests for the coding challenges to ensure that exceptions are caught and bugs are fixed.
 */
package io.catalyte.training;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class LogicProblemsImplTest {

    LogicProblemsImpl logicExercise = new LogicProblemsImpl();

    /**
     * This test is happy path that checks to see if the code will return the expected answer
     */
    @Test
    void averageReturnsDoubleTwoDecimalPlaces() {
        int[] scores = {1, 4, 11, 33, 75, 89};

        double result = logicExercise.average(scores);

        double expected = 35.50;

        Assertions.assertEquals(expected, result, () -> "The wrong double was returned. Expected: " + expected + ", Got: " + result);

      }

    /**
     * This test checks to see if an exception will be thrown if there is a negative element present in the
     * scores array and checks to see if the expected error message is displayed
     */
    @Test
    void averageNegativeElementShouldThrowIllegalArgumentException() {
        int[] scores = {5, -26, 8, 32, 19, 82};

        Assertions.assertThrows(IllegalArgumentException.class, () -> logicExercise.average(scores));

    }

    /**
     * This test checks to see if an exception will be thrown if there is a null value for scores array
     */
    @Test
    void arrayEqualsNullShouldThrowNullPointerException() {
        Exception ex = Assertions.assertThrows(NullPointerException.class, () -> logicExercise.average(null));

        String expectedMessage = "scores cannot equal null. Must contain integers";
        Assertions.assertEquals(expectedMessage, ex.getMessage(), () -> "Message did not equal " + expectedMessage);
    }

    /**
     * This test checks to if an empty array is entered the code will return a double of "0.00"
     */
    @Test
    void averageEmptyArrayShouldReturnZeroWithTwoDecimalSpaces() {
        int[] scores = {};

        double result = logicExercise.average(scores);

        double expected = 0.00;

        Assertions.assertEquals(expected, result, () -> "The wrong double was returned. Expected: " + expected + ", Got: " + result);
    }

    /**
     * This test checks to see if when a there is only one value in the array the method should return
     * that value with two decimal spaces
     */
    @Test
    void averageSingleArrayElementShouldReturnWithTwoDecimalSpaces() {
        int[] scores = {4};

        double result = logicExercise.average(scores);

        double expected = 4.00;

        Assertions.assertEquals(expected, result, () -> "The wrong double was returned. Expected: " + expected + ", Got: " + result);
    }

    /**
     * This test checks to see if an exception will be thrown if there is a empty string value in an array
     */
    @Test
    void emptyStringShouldThrowIllegalArgumentException() {
        String string = "";
        Exception ex = Assertions.assertThrows(IllegalArgumentException.class, () -> logicExercise.lastWordLength(string));

        String expectedMessage = "input must not be an empty string";
        Assertions.assertEquals(expectedMessage, ex.getMessage(), () -> "Message did not equal " + expectedMessage);
    }

    /**
     * This test is a happy path test that checks to see if the length of the last word in a string will be returned
     * as an integer
     */
    @Test
    void lengthOfLastWordShouldReturnInt() {
        String string = "I though it was going to be different";
        int expected = 9;
        int result = logicExercise.lastWordLength(string);

        Assertions.assertEquals(expected, result, () -> "The wrong integer was returned. Expected: " + expected + ", Got: " + result);
    }

    /**
     * This test is to check if a string of only whitespace will return an integer 0
     */
    @Test
    void stringOfOnlyWhitespaceShouldReturnZero() {
        String string = "         ";

        int result = logicExercise.lastWordLength(string);

        int expected = 0;

        Assertions.assertEquals(expected, result, () -> "The wrong integer was returned. Expected: " + expected + ", Got: " + result);
    }

    /**
     * This test is to check if the String array in the argument is empty, the method will return an empty List array
     */
    @Test
    void emptyStringArrayShouldReturnEmptyLists() {
        List<List<String>> expected = new ArrayList<>();
        String[] arrayStrings = {};
        List<List<String>> result = logicExercise.groupStrings(arrayStrings);

        Assertions.assertEquals(expected, result, () -> "The wrong List was returned. Expected: " + expected + ", Got: " + result);

      }

    /** This test is a happy path to check if when given an array of strings, it will sort out the strings will the
     * first letter of "a" and last letter of "e", and put them in one list. Then sort out the strings will the
     * first letter of "a" and last letter of "t", and put them in another list. Then finally adding those
     * lists to another list.
     */
    @Test
    void shouldReturnListOfListsContainingStringsWithTheSameFirstAndLastLetters() {
        List<List<String>> expected = new ArrayList<>();
        List<String> testListOne = new ArrayList<>();
        List<String> testListTwo = new ArrayList<>();

        String[] stringArray = {"assault", "arrange", "arrogant", "act", "assert", "ace", "assume", "assistance"};

        testListOne.add("arrange");
        testListOne.add("ace");
        testListOne.add("assume");
        testListOne.add("assistance");

        testListTwo.add("assault");
        testListTwo.add("arrogant");
        testListTwo.add("act");
        testListTwo.add("assert");

        expected.add(testListOne);
        expected.add(testListTwo);

        List<List<String>> result = logicExercise.groupStrings(stringArray);

        Assertions.assertEquals(expected, result, () -> "The wrong List was returned. Expected: " + expected + ", Got: " + result);
    }

    /**
     * This test is to return expected result even if strings in the array contain whitespace before and after string characters
     */
    @Test
    void returnsExpectedResultEvenWithStringsWithBeginningOrTrailingWhitespace() {
        List<List<String>> expected = new ArrayList<>();
        List<String> testListOne = new ArrayList<>();
        List<String> testListTwo = new ArrayList<>();

        String[] stringArray = {"  assault  ", "   arrange   ", "  arrogant  ", "act", " assert", "assume ", "assistance"};

        testListOne.add("arrange");
        testListOne.add("assume");
        testListOne.add("assistance");

        testListTwo.add("assault");
        testListTwo.add("arrogant");
        testListTwo.add("act");
        testListTwo.add("assert");

        expected.add(testListOne);
        expected.add(testListTwo);

        List<List<String>> result = logicExercise.groupStrings(stringArray);

        Assertions.assertEquals(expected, result, () -> "The wrong List was returned. Expected: " + expected + ", Got: " + result);
      }

    /**
     * This test checks to see if an exception will be thrown if there is an empty string element in an array and
     * to check that the expected message is displayed
      */
    @Test
    void emptyStringArrayShouldThrowIllegalArgumentException() {

        String[] stringArray = {"assault", "arrange", "arrogant", "act", "", "ace", "assume", "assistance"};

        Assertions.assertThrows(IllegalArgumentException.class, () -> logicExercise.groupStrings(stringArray));
    }

    /**
     * This test is a happy path that checks that the calculated number of distinct paths returns the expected
     * value
     */
    @Test
    void distinctLadderPathMethodShouldReturnExpectedResult() {
        BigDecimal expected = new BigDecimal("573147844013817084101");

        BigDecimal result = logicExercise.distinctLadderPaths(100);

        Assertions.assertEquals(expected, result, () -> "The wrong BigDecimal was returned. Expected: " + expected + ", Got: " + result);
    }

    /**
     * This test checks if the value of rungs is 0, that the method will return a BigDecimal of 0
     */
    @Test
    void distinctLadderPathMethodShouldReturnZero() {
        BigDecimal expected = new BigDecimal(0);

        BigDecimal result = logicExercise.distinctLadderPaths(0);

        Assertions.assertEquals(expected, result, () -> "The wrong BigDecimal was returned. Expected: " + expected + ", Got: " + result);
    }

    /**
     *  This test checks to see if an exception will be thrown if there is a negative element value for the rungs parameter
     *  and checks to see if the expected error message is displayed
     */
    @Test
    void distinctLadderPathMethodShouldThrowIllegalArgumentException() {
        Exception ex = Assertions.assertThrows(IllegalArgumentException.class, () -> logicExercise.distinctLadderPaths(-21));

        String expectedMessage = "ladders can't have negative rungs";
        Assertions.assertEquals(expectedMessage, ex.getMessage(), () -> "Message did not equal " + expectedMessage);
    }
}