/**
 * @author - Don Moore
 * @version - Intermediate programming project v1
 * @task - Create a code challenge which involves writing code challenges from the Logic Problem interface.
 *       - We are to write tests for the coding challenges to ensure that exceptions are caught and bugs are fixed.
 */
package io.catalyte.training;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.*;

public class LogicProblemsImpl implements LogicProblems {
    /**
     * This method is used to find the average of a list of scores
     * @param scores - the list of scores to averaged out
     * @return - Returns an average of the total scores as a double type
     */
    @Override
    public Double average(int[] scores) {
        // If array of scores is null, throw exception
        if (scores == null) {
            throw new NullPointerException("scores cannot equal null. Must contain integers");
        }
        // Create two space decimal format
        DecimalFormat twoDecimalPlaces = new DecimalFormat("0.00");
        double average;
        double sum = 0;
        String rawAverage;
        // If the scores array is empty, return 0.00
        if (scores.length == 0) {
            return 0.00;
        }
        // Iterate through scores array and add all the scores together
        for (double score: scores) {
            // If the score is a negative number throw exception
            if (score < 0) {
                throw new IllegalArgumentException("scores must be positive");
            } else {
                sum += score;
            }
        }
        // Take average and format it for two decimal spaces
        rawAverage = twoDecimalPlaces.format(sum / scores.length);
        // Convert average to a double
        average = Double.parseDouble(rawAverage);

        return average;
    }

    /**
     * This method is for when given a string, it will return the length of the last word in the string.
     * @param text - The words entered as a string
     * @return - Returns the length of the last word as a integer
     */
    @Override
    public int lastWordLength(String text) {
        // If the string is empty, throw an exception
        if (text.length() == 0) {
            throw new IllegalArgumentException("input must not be an empty string");
        }
        int lengthOfLastWord;
        // If the text is only whitespace, return 0
        if (text.matches("/^\s*$/")) {return 0;}
        // Separate words and put them in an array
        String[] stringArray = text.trim().split(" ");

        // Find the last word by accessing the last index of the array
        lengthOfLastWord = stringArray[stringArray.length - 1].length();

        return lengthOfLastWord;
    }

    /**
     * This method is for when given the number of rungs for a ladder, determine the number of distinct paths to reach the top of the ladder
     * @param rungs - An for the number of steps
     * @return - Returns a BigDecimal of the number of distinct paths taken to climb the ladder to the top rung
     */
    @Override
    public BigDecimal distinctLadderPaths(int rungs) {
        // If the number of rungs is negative, thrown an exception
        if (rungs < 0) {
            throw new IllegalArgumentException("ladders can't have negative rungs");
        }
        // If the number of rungs is zero, return a value of 0
        if (rungs == 0) {
            return BigDecimal.valueOf(0);
        }
        // If the number of rungs equal to 2 or 1, return the number of the rungs
        if (rungs <= 2) { return BigDecimal.valueOf(rungs);
        } else {
            // Create an array of store the calculated fibonacci numbers to reuse to increase the efficiency of calculating all of the fibonacci numbers
            BigInteger[] fibonacciCache = new BigInteger[(rungs + 1)];
            fibonacciCache[0] = BigInteger.valueOf(1);
            fibonacciCache[1] = BigInteger.valueOf(1);
            // Loop through the rungs and as we calculate the fibonacci numbers, add the to the fibonacciCache array to be reused
            for (int i = 2; i <= rungs; i++) {
                fibonacciCache[i] = fibonacciCache[i - 1].add(fibonacciCache[i - 2]);
            }
            // Convert the number of distinct paths into a BigDecimal and return it
            return new BigDecimal(fibonacciCache[rungs]);
        }
    }

    /**
     * Given I have an array of strings, when I call the group strings method, then I receive a List of Lists
     * that each contain properly grouped strings. All strings within a grouping must share the same first
     * letters and the same last letters.
     * @param strs - The given array of strings
     * @return - Returns a list of lists with the first list containing all the words that start with "a"
     * and end with "e" and the second list containing all the words that start with "a" and end with "t"
     */
    @Override
    public List<List<String>> groupStrings(String[] strs) {
        List<List<String>> reverseList;
        List<List<String>> finalList = new ArrayList<>();
        HashMap<String, List<String>> subList = new HashMap<>();


        if (strs.length == 0) { return finalList; }
        // Take first word get first and last letters
        for (String string : strs) {
            if (Objects.equals(string, "")) {
                throw new IllegalArgumentException("strings must not be empty");
            }
            String strippedString = string.stripLeading().stripTrailing();
            String combineLetters = strippedString.charAt(0) + strippedString.substring(strippedString.length() - 1);
            if (subList.containsKey(combineLetters)) {
                subList.get(combineLetters).add(strippedString);
            } else {
                List<String> newArrayList = new ArrayList<>();
                newArrayList.add(strippedString);
                subList.put(combineLetters, newArrayList);
            }
        }
        reverseList = subList.values().stream().toList();
        reverseList.forEach(l -> finalList.add(0, l));

        return finalList;
    }
}
