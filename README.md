# Intermediate Programming Project Template

# Description

This project is create to be a a take home coding challenge. We are required to show how we implement the interface. 
Create and solve the coding challenges, and the test the answers so that the code challenge will not crash and will ensure that it
tests for the correct answers. The challenges are found in src > main > java > io.catalyte.training > LogicProblemsImpl.java.

# Testing Section

This section is where the tests are stored and where you will test the solutions to the coding challenges. 
The test code can be found in The challenges are found in src > test > java > io.catalyte.training > LogicProblemsImplTest.java.

# Run Tests

Open your Intellij Idea app. Clone the file from the Gitlab repo. When the file opens locate the LogicProblemsImplTest.java
file and open it. In the left column fo the editor, there will be green play icons which match up to each of the testing 
methods. To run a particular test, click on the corresponding play icon and select the first run option. To run all the tests at once, click the double play 
icon that corresponds with "public class LogicProblemsImplTest". To run tests with coverage, choose the "run with coverage option" after clicking play icon. Test results will be displayed in the bottom console of the 
Intellij Idea app.

```bash
git clone https://gitlab.com/dondev1992/intermediate-programming-project.git
```

Intermediate Programming Project Template is used to give trainees a base project for demonstrating logical thinking. 

## Installation

Fork the repository to your specified work directory, then clone it down

```bash
git clone <insert_url_here>
```

## Usage
Fill out LogicProblemsImpl with working solutions to the interface. Follow all requirements specified in the project instructions.

## Contributing
Feedback is welcome. If you see issues, or have ideas for improvement, please submit feedback!